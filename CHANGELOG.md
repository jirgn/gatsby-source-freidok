# 1.0.0 (2021-04-28)


### Bug Fixes

* add tests for plugin-options ([64737a9](https://gitlab.com/jirgn/gatsby-source-freidok/commit/64737a97f1b3fdda684369ff06d27b44154505da))
* example page query and content ([e40121d](https://gitlab.com/jirgn/gatsby-source-freidok/commit/e40121dbffca105750b3fea5091dc39a036c2d2d))


### Features

* add more data to publication ([707fd98](https://gitlab.com/jirgn/gatsby-source-freidok/commit/707fd98b1ed021ac8d4b753785e13f5c0ca24e72))
* source fdpublications ([1b272b3](https://gitlab.com/jirgn/gatsby-source-freidok/commit/1b272b31ce9ca0c21aac086d5945662ab9588e4a))
