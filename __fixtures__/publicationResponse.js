/*
  http 'https://freidok.uni-freiburg.de/jsonApi/v1/publications?publicationId=166546&maxPers=0&field=id,link,abstracts,pub_ids,pubtype,languages,publication_year,day_of_exam,system_time,titles,title_parents,persons,partof,source,contract,license_metadata,preview_image,files_stat,files,issued&lang=en' | jq | tee /dev/tty | wl-copy
 */
const publicationsAll = {
  numFound: 1,
  start: 0,
  maxRows: 20,
  type: 'publication',
  docs: [
    {
      id: 166546,
      link: 'https://freidok.uni-freiburg.de/data/166546',
      abstracts: [
        {
          language: 'eng',
          language_value: 'English',
          value:
            'The complex phosphorylation pattern of natural and modified pentaphosphorylated magic spot nucleotides is generated in a highly efficient way. A cyclic pyrophosphoryl phosphoramidite (cPyPA) reagent is used to introduce four phosphates on nucleosides regioselectively in a one-flask key transformation. The obtained magic spot nucleotides are used to develop a capillary electrophoresis UV detection method, enabling nucleotide assignment in complex bacterial extracts.',
        },
      ],
      pub_ids: [
        {
          type: 'doi',
          type_value: 'DOI',
          value: '10.1021/acs.joc.0c00841',
          link: 'https://doi.org/10.1021/acs.joc.0c00841',
        },
        {
          type: 'urn',
          type_value: 'URN',
          value: 'urn:nbn:de:bsz:25-freidok-1665462',
          link: 'https://nbn-resolving.org/urn:nbn:de:bsz:25-freidok-1665462',
        },
        {
          type: 'ppn',
          type_value: 'PPN',
          value: '1752468473',
          link: 'https://katalog.ub.uni-freiburg.de/link?kid=1752468473',
        },
        {
          type: 'dnb',
          type_value: 'DNB',
          value: '1230321780',
          link: 'https://d-nb.info/1230321780',
        },
      ],
      pubtype: {
        type: 'article',
        value: 'Scientific article',
      },
      languages: [
        {
          type: 'eng',
          value: 'English',
        },
      ],
      publication_year: {
        value: 2020,
      },
      system_time: {
        date_submitted: {
          timestamp: 1595333620,
          value: '21.07.2020 14:13',
        },
        date_updated: {
          timestamp: 1616685586,
          value: '25.03.2021 16:19',
        },
        date_issued: {
          timestamp: 1595394640,
          value: '22.07.2020 07:10',
        },
      },
      titles: [
        {
          language: 'eng',
          language_value: 'English',
          value:
            'Four phosphates at one blow: access to pentaphosphorylated magic spot nucleotides and their analysis by capillary electrophoresis',
        },
      ],
      title_parents: [],
      persons: [
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 18749,
          link: 'https://freidok.uni-freiburg.de/pers/18749',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
          ],
          author_function: {
            type: 'first_author',
            value: 'First author',
            sup_no: 'a',
          },
          gnd_link: 'https://d-nb.info/gnd/1127883690',
          orcid_link: 'https://orcid.org/',
          forename: 'Thomas',
          surname: 'Haas',
          value: 'Haas, Thomas',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 209135,
          link: 'https://freidok.uni-freiburg.de/pers/209135',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
          ],
          author_function: {
            type: 'equally_contributed_author',
            value: 'Author equally contributed',
            sup_no: 'b',
          },
          gnd_link: 'https://d-nb.info/gnd/1214307752',
          orcid_link: 'https://orcid.org/0000-0003-2197-3218',
          forename: 'Danye',
          surname: 'Qiu',
          value: 'Qiu, Danye',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 46744,
          link: 'https://freidok.uni-freiburg.de/pers/46744',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/1214307906',
          orcid_link: 'https://orcid.org/',
          forename: 'Markus',
          surname: 'Häner',
          value: 'Häner, Markus',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 270327,
          link: 'https://freidok.uni-freiburg.de/pers/270327',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2051,
              value: 'Institute for Biochemistry and Molecular Biology',
              sup_no: 2,
              link: 'https://freidok.uni-freiburg.de/inst/2051',
              lifetime: '1995 - ',
            },
            {
              norm_id: 1014,
              value: 'Medizinische Fakultät',
              sup_no: 3,
              link: 'https://freidok.uni-freiburg.de/inst/1014',
              lifetime: '',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/1214308066',
          orcid_link: 'https://orcid.org/',
          forename: 'Larissa',
          surname: 'Angebauer',
          value: 'Angebauer, Larissa',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 25305,
          link: 'https://freidok.uni-freiburg.de/pers/25305',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/121349687X',
          orcid_link: 'https://orcid.org/0000-0001-6454-4697',
          forename: 'Alexander',
          surname: 'Ripp',
          value: 'Ripp, Alexander',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 96738,
          link: 'https://freidok.uni-freiburg.de/pers/96738',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/1205491759',
          orcid_link: 'https://orcid.org/',
          forename: 'Jyoti',
          surname: 'Singh',
          value: 'Singh, Jyoti',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 12861,
          link: 'https://freidok.uni-freiburg.de/pers/12861',
          is_corresponding_author: false,
          affiliations: [
            {
              norm_id: 2051,
              value: 'Institute for Biochemistry and Molecular Biology',
              sup_no: 2,
              link: 'https://freidok.uni-freiburg.de/inst/2051',
              lifetime: '1995 - ',
            },
            {
              norm_id: 1014,
              value: 'Medizinische Fakultät',
              sup_no: 3,
              link: 'https://freidok.uni-freiburg.de/inst/1014',
              lifetime: '',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/1099709261',
          orcid_link: 'https://orcid.org/0000-0001-5913-0334',
          forename: 'Hans-Georg',
          surname: 'Koch',
          value: 'Koch, Hans-Georg',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 23536,
          link: 'https://freidok.uni-freiburg.de/pers/23536',
          is_corresponding_author: true,
          affiliations: [
            {
              norm_id: 2584,
              value: 'Department of Pharmaceutical Biology and Biotechnology',
              sup_no: 4,
              link: 'https://freidok.uni-freiburg.de/inst/2584',
              lifetime: '2005 - ',
            },
          ],
          author_function: null,
          gnd_link: 'https://d-nb.info/gnd/1160091412',
          orcid_link: 'https://orcid.org/0000-0003-4216-8189',
          forename: 'Claudia',
          surname: 'Jessen-Trefzer',
          value: 'Jessen-Trefzer, Claudia',
        },
        {
          type: 'author',
          type_value: 'Author',
          norm_id: 24717,
          link: 'https://freidok.uni-freiburg.de/pers/24717',
          is_corresponding_author: true,
          affiliations: [
            {
              norm_id: 2448,
              value: 'Institut für Organische Chemie',
              sup_no: 1,
              link: 'https://freidok.uni-freiburg.de/inst/2448',
              lifetime: '20.09.2012 - ',
            },
            {
              norm_id: 3203,
              value:
                'CIBSS - Centre for Integrative Biological Signalling Studies',
              sup_no: 5,
              link: 'https://freidok.uni-freiburg.de/inst/3203',
              lifetime: '',
            },
          ],
          author_function: {
            type: 'last_author',
            value: 'Last author',
            sup_no: 'c',
          },
          gnd_link: 'https://d-nb.info/gnd/136133185',
          orcid_link: 'https://orcid.org/0000-0002-1025-9484',
          forename: 'Henning',
          surname: 'Jessen',
          value: 'Jessen, Henning',
        },
      ],
      partof: [
        {
          type: 'journal',
          title: 'The journal of organic chemistry',
          source_id: '1520-6904',
          source_id_type: 'issn',
          source_id_type_value: 'ISSN',
          link: 'https://katalog.ub.uni-freiburg.de/link?ss=1520-6904',
          place: 'Washington, DC [u.a.]',
          publisher: 'American Chemical Society (ACS)',
          year: 2020,
          volume: '85',
          issue: '22',
          page: '14496–14506',
        },
      ],
      license_metadata: {
        type: 'cc_0',
        type_value: 'Creative Commons CC0',
        link: 'http://creativecommons.org/about/cc0',
      },
      files_stat: {
        public_count: 1,
      },
      files: [
        {
          type: 'document',
          type_value: 'publishing',
          label: null,
          filename: 'acs.joc.0c00841.pdf',
          link:
            'https://freidok.uni-freiburg.de/fedora/objects/freidok:166546/datastreams/FILE1/content',
          filesize: 2152531,
          filesize_display: '2.05 MB',
          preview_image: '',
          md5sum: 'fb4c5717daa673b3dcb0d638ea3c2ea3',
          license: {
            type: 'license_existing',
            type_value:
              'Takes over regulations of the licenser or the (Creative Commons) license stated in the document itself.',
            cc_by_name: null,
            link: null,
          },
        },
      ],
      issued: {
        value: true,
      },
    },
  ],
};

module.exports = {
  publicationsAll,
};
