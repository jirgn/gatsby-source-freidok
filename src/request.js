const axios = require('axios');

function request(path, config) {
  return axios.get(path, {
    baseURL: 'https://freidok.uni-freiburg.de/jsonApi/v1',
    headers: {
      Accept: 'application/json',
    },
    ...config,
  });
}

async function requestPublications(queryParameters) {
  console.info('freidok - requesting publications for', queryParameters);

  if (!queryParameters) {
    throw Error(
      'no filter queryParameters with filter citeria for publications given. Specify e.g. {publicationId: <value>}'
    );
  }
  const requestParameters = {
    ...queryParameters,
    maxPers: 0,
    field:
      'id,link,abstracts,pub_ids,pubtype,publication_year,system_time,titles,persons,partof,license_metadata,issued',
    lang: 'en',
  };

  const result = await request('/publications', { params: requestParameters });
  return result.data;
}

module.exports = {
  requestPublications,
};
