function normalizePublications(publicationsResponse) {
  const publications = publicationsResponse.docs ?? [];

  return publications.map(publication => ({
    id: publication.id,
    publicIds: publication.pub_ids
      ? publication.pub_ids.map(id => ({
          type: id.type,
          value: id.value,
          link: id.link,
        }))
      : [],
    link: publication.link,
    abstracts: publication.abstracts
      ? publication.abstracts.map(a => ({
          language: a.language,
          value: a.value,
        }))
      : [],
    type: publication.pubtype?.type,
    publicationYear: publication.publication_year?.value,
    systemTime: publication.system_time
      ? {
          dateSubmitted: publication.system_time.date_submitted?.timestamp,
          dateUpdated: publication.system_time.date_updated?.timestamp,
          dateIssued: publication.system_time.date_issued?.timestamp,
        }
      : null,
    titles: publication.titles
      ? publication.titles.map(a => ({
          language: a.language,
          value: a.value,
        }))
      : [],
    persons: publication.persons
      ? publication.persons.map(p => ({
          type: p.type,
          link: p.link,
          gndLink: p.gnd_link,
          orcidLink: p.orcid_link,
          isCorrespondingAuthor: p.is_corresponding_author,
          authorFunction: p.author_function?.type,
          forename: p.forename,
          surname: p.surname,
        }))
      : [],
    partOf: publication.partof
      ? publication.partof.map(p => ({
          type: p.type,
          title: p.title,
          publisher: p.publisher,
        }))
      : null,
    issued: publication.issued ? publication.issued.value : false,
    license: publication.license_metadata?.type_value,
  }));
}

module.exports = {
  normalizePublications,
};
