import { graphql } from 'gatsby';
import React from 'react';

export default ({ data: { allFdPublication } }) => {
  return (
    <div>
      <h1>Freidok Source Plugin</h1>
      <h2>Publications</h2>
      {allFdPublication.nodes.map((node, index) => (
        <div key={index}>
          <h3>{node.titles?.length && node.titles[0].value}</h3>
          <dl>
            <dt>type</dt>
            <dd>{node.type}</dd>
          </dl>
          <p>{node.abstracts?.length && node.abstracts[0].value}</p>
        </div>
      ))}
    </div>
  );
};

export const pageQuery = graphql`
  query IndexQuery {
    allFdPublication {
      nodes {
        link
        titles {
          value
        }
        type
        abstracts {
          value
        }
      }
      totalCount
    }
  }
`;
