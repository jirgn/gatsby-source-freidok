module.exports = {
  plugins: [
    {
      // resolve: "gatsby-source-freidok",
      resolve: require.resolve('..'),
      options: {
        publicationsQuery: {
          persId: 23536
        }
      },
    },
  ],
};
