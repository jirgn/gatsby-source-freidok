const { testPluginOptionsSchema } = require('gatsby-plugin-utils');
const { pluginOptionsSchema } = require('../gatsby-node');

describe('pluginOptionsSchema', () => {
  describe('valid options', () => {
    it('should validate correct single options', async () => {
      const options = {
        publicationsQuery: {
          publicationId: '12345',
          doi: '12.3456/fmicb.2020.570606',
          persId: '1234',
          persRole: '1234$author',
          pubType: 'article',
          instId: '12345',
          projId: '12345'
        }
      };
      const { isValid, errors } = await testPluginOptionsSchema(
        pluginOptionsSchema,
        options
      );
      expect(isValid).toBe(true);
      expect(errors).toEqual([]);
    });
    it('should validate correct multi options', async () => {
      const options = {
        publicationsQuery: {
          publicationId: '12345, 23456',
          doi: '12.3456/fmicb.2020.570606, 12.3456/fmicb.2020.570606',
          persId: '1234, 2345',
          persRole: '1234$author, 2345$illustrator',
          instId: '12345, 23456',
          projId: '12345, 23456'
        }
      };
      const { isValid, errors } = await testPluginOptionsSchema(
        pluginOptionsSchema,
        options
      );
      expect(isValid).toBe(true);
      expect(errors).toEqual([]);
    });
  });
  it('should invalidate incorrect options', async () => {
    const options = {
      otherOption: 'someting not allowed',
      publicationsQuery: {
        unknownQueryParam: 'invalid query param',
        publicationId: 'incorrectId',
        doi: 'incorrectId',
        persId: 'incorrectId',
        persRole: 'unknownRole',
        pubType: 'someOtherType',
        instId: 'incorrectId',
        projId: 'incorrectId',
      },
    };
    const { isValid, errors } = await testPluginOptionsSchema(
      pluginOptionsSchema,
      options
    );
    expect(isValid).toBe(false);
    expect(errors).toEqual([
      '"publicationsQuery.publicationId" with value "incorrectId" fails to match the one or more publication identifiers pattern',
      '"publicationsQuery.doi" with value "incorrectId" fails to match the one or multiple doi identifiers pattern',
      '"publicationsQuery.persId" with value "incorrectId" fails to match the one or more person identifiers pattern',
      '"publicationsQuery.persRole" with value "unknownRole" fails to match the one or more person roles pattern',
      '"publicationsQuery.pubType" must be one of [doctoral_thesis, article, book_part, conference_object, other_doctype, working_paper, bachelor_thesis, article_in_conference_proceedings, contribution_to_periodical, image, book, service, diploma_thesis, emulation, annotation, research_data, habilitation_thesis, manuscript, hierarchical_object, interactive_resource, carthographic_material, course_material, magister_thesis, master_thesis, model, musical_notation, patent, physical_object, poster, preprint, review, collection, software, state_exam, study_thesis, conference_proceedings, text, sound, report, moving_image, lecture, speech, website, workflow, periodical]',
      '"publicationsQuery.instId" with value "incorrectId" fails to match the one or more institution identifiers pattern',
      '"publicationsQuery.projId" with value "incorrectId" fails to match the one or more project identifiers pattern',
      '"publicationsQuery.unknownQueryParam" is not allowed',
      '"otherOption" is not allowed',
    ]);
  });
});
