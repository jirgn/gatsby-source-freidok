const { requestPublications } = require('./src/request');
const { normalizePublications } = require('./src/normalize');

const PUBLICATION_NODE_TYPE = 'FdPublication';

exports.onPreInit = () => {
  console.info('Loading gatsby-source-freidok');
};

exports.pluginOptionsSchema = ({ Joi }) => {
  return Joi.object({
    publicationsQuery: Joi.object({
      publicationId: Joi.string()
        .pattern(
          /\d{4,8}(?:\s*,\s*\d{4,8})*/,
          'one or more publication identifiers'
        )
        .description('one or multiple ids seperated by comma'),
      doi: Joi.string()
        .pattern(
          /\d{2}\.\d{4}\/.+(?:,\d{2}\.\d{4}\/.+)*/,
          'one or multiple doi identifiers'
        )
        .description('one'),
      persId: Joi.string()
        .pattern(/\d{4,8}(?:\s*,\s*\d{4,8})*/, 'one or more person identifiers')
        .description('one or multiple ids seperated by comma'),
      persRole: Joi.string()
        .pattern(
          /\d{4,8}\$(?:author|advisor|reviewer|contact_person|celebrated_person|editor|editing|hrsg|researcher|sponsor_person|producer|rightsholder_person|bibliographer|conductor|illustrator|interviewee|layout|protocol|translator|research_group|data_manager|workpackageleader|other_person)(?:\s*,\s*\d{4,8}\$(?:author|advisor|reviewer|contact_person|celebrated_person|editor|editing|hrsg|researcher|sponsor_person|producer|rightsholder_person|bibliographer|conductor|illustrator|interviewee|layout|protocol|translator|research_group|data_manager|workpackageleader|other_person))*/,
          'one or more person roles'
        )
        .description(
          'one or multiple roles of personid like 1234$author seperated by comma'
        ),
      pubType: Joi.string()
        .valid(
          'doctoral_thesis',
          'article',
          'book_part',
          'conference_object',
          'other_doctype',
          'working_paper',
          'bachelor_thesis',
          'article_in_conference_proceedings',
          'contribution_to_periodical',
          'image',
          'book',
          'service',
          'diploma_thesis',
          'emulation',
          'annotation',
          'research_data',
          'habilitation_thesis',
          'manuscript',
          'hierarchical_object',
          'interactive_resource',
          'carthographic_material',
          'course_material',
          'magister_thesis',
          'master_thesis',
          'model',
          'musical_notation',
          'patent',
          'physical_object',
          'poster',
          'preprint',
          'review',
          'collection',
          'software',
          'state_exam',
          'study_thesis',
          'conference_proceedings',
          'text',
          'sound',
          'report',
          'moving_image',
          'lecture',
          'speech',
          'website',
          'workflow',
          'periodical'
        )
        .description('type of publication'),
      instId: Joi.string()
        .pattern(
          /\d{4,8}(?:\s*,\s*\d{4,8})*/,
          'one or more institution identifiers'
        )
        .description('one or multiple ids seperated by comma'),
      projId: Joi.string()
        .pattern(
          /\d{4,8}(?:\s*,\s*\d{4,8})*/,
          'one or more project identifiers'
        )
        .description('one or multiple ids seperated by comma'),
    })
      .required()
      .description(
        'publications query - subset of official API https://freidok.uni-freiburg.de/site/interfaces'
      ),
  });
};

exports.sourceNodes = async (
  { actions, createContentDigest, createNodeId },
  pluginOptions
) => {
  const { createNode } = actions;
  const { publicationsQuery } = pluginOptions;
  const publicationsResponse = await requestPublications(publicationsQuery);
  const publications = normalizePublications(publicationsResponse);

  for (const publication of publications) {
    createNode({
      ...publication,
      id: createNodeId(`${PUBLICATION_NODE_TYPE}-${publication.id}`),
      parent: null,
      children: [],
      internal: {
        type: PUBLICATION_NODE_TYPE,
        content: JSON.stringify(publication),
        contentDigest: createContentDigest(publication),
      },
    });
  }
};

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions;

  createTypes(`
    enum FdIdentifierTypeEnum {
      doi,
      urn,
      ppn,
      dnb
    }

    enum FdPublicationTypeEnum {
      doctoral_thesis,
      article,
      book_part,
      conference_object,
      other_doctype,
      working_paper,
      bachelor_thesis,
      article_in_conference_proceedings,
      contribution_to_periodical,
      image,
      book,
      service,
      diploma_thesis,
      emulation,
      annotation,
      research_data,
      habilitation_thesis,
      manuscript,
      hierarchical_object,
      interactive_resource,
      carthographic_material,
      course_material,
      magister_thesis,
      master_thesis,
      model,
      musical_notation,
      patent,
      physical_object,
      poster,
      preprint,
      review,
      collection,
      software,
      state_exam,
      study_thesis,
      conference_proceedings,
      text,
      sound,
      report,
      moving_image,
      lecture,
      speech,
      website,
      workflow,
      periodical
    }

    type FdIdentifier {
      type: FdIdentifierTypeEnum!
      value: String!
      link: String!
    }

    type FdSystemTime {
      dateSubmitted: Int
      dateUpdated: Int
      dateIssued: Int
    }

    type FdLanguageAwareValue {
      language: String!
      value: String
    }

    type FdContributor {
      type: String!
      link: String!
      gndLink: String
      orcidLink: String
      isCorrespondingAuthor: Boolean!
      authorFunction: String
      forename: String!
      surname: String!
    }

    type FdPartOf {
      type: String!
      title: String!
      publisher: String
    }

    type ${PUBLICATION_NODE_TYPE} implements Node {
      id: String!
      publicIds: [ FdIdentifier ]
      link: String!
      abstracts: [ FdLanguageAwareValue! ]
      type: FdPublicationTypeEnum
      publicationYear: Int
      systemTime: FdSystemTime
      titles: [ FdLanguageAwareValue! ]
      persons: [ FdContributor ]
      partOf: [ FdPartOf ]
      issed: Boolean
      license: String
    }
  `);
};
